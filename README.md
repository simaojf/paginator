# Pagination

Pagination project in python.

## Readme Notes

* Command line starts with $, the command should run with user privileges

## Retrieve code
* `$ git clone https://simaojf@bitbucket.org/simaojf/paginator.git`

## Testing

* `$ python -m unittest`