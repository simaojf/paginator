#!/usr/bin/python


def pagination(data):
    """
    Generates the series of links to the pages in a paginated list.
    """
    # Ensures that all variables are valid integers.
    try:
        current_page = int(data['$current_page'])
        total_pages = int(data['$total_pages'])
        boundaries = int(data['$boundaries'])
        around = int(data['$around'])
    except (KeyError, ValueError):
        return ''

    # Ensures that all variable are positives.
    if current_page < 0 or total_pages < 0 or boundaries < 0 or around < 0:
        return ''

    # The current page, boundaries and around must be in range.
    if current_page > total_pages or boundaries > total_pages or around > total_pages:
        return ''

    # Calculate the pagination.
    pages = (set(range(1, boundaries + 1)) |
             set(range(max(1, current_page - around),
                       min(current_page + around + 1, total_pages + 1))) |
             set(range(total_pages - boundaries + 1, total_pages + 1)))

    # Add delimiters (...) to pagination, wherever there's a gap between pages.
    counter = 0
    temp_pages = []
    for page in sorted(pages):
        if page != counter + 1 and boundaries != 0:
            temp_pages.append('...')
        temp_pages.append(str(page))
        counter = page

    # Return str with the pagination.
    return ' '.join(temp_pages)
