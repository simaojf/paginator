import unittest
from paginator import pagination


class PaginatorTestCase(unittest.TestCase):

    # Values for success cases.
    VALID_DATA = [
        {
            'data': {
                '$current_page': 4,
                '$total_pages': 5,
                '$boundaries': 1,
                '$around': 0
            },
            'result': '1 ... 4 5'
        },
        {
            'data': {
                '$current_page': 4,
                '$total_pages': 10,
                '$boundaries': 2,
                '$around': 2
            },
            'result': '1 2 3 4 5 6 ... 9 10'
        },
        {
            'data': {
                '$current_page': 10,
                '$total_pages': 20,
                '$boundaries': 3,
                '$around': 3
            },
            'result': '1 2 3 ... 7 8 9 10 11 12 13 ... 18 19 20'
        },
        {
            'data': {
                '$current_page': 1,
                '$total_pages': 10,
                '$boundaries': 2,
                '$around': 0
            },
            'result': '1 2 ... 9 10'
        },
        {
            'data': {
                '$current_page': 10,
                '$total_pages': 10,
                '$boundaries': 2,
                '$around': 0
            },
            'result': '1 2 ... 9 10'
        },
        {
            'data': {
                '$current_page': 5,
                '$total_pages': 10,
                '$boundaries': 0,
                '$around': 0
            },
            'result': '5'  # weird but valid
        },
        {
            'data': {
                '$current_page': 5,
                '$total_pages': 10,
                '$boundaries': 0,
                '$around': 1
            },
            'result': '4 5 6'  # weird but valid
        }
    ]

    # Values for invalid cases.
    INVALID_DATA = [
        {
            'data': {
                '$current_page': 'a',  # not a int
                '$total_pages': 5,
                '$boundaries': 1,
                '$around': 0
            },
            'result': ''
        },
        {
            'data': {
                '$current_page': -4,  # not positive
                '$total_pages': 10,
                '$boundaries': 2,
                '$around': 2
            },
            'result': ''
        },
        {
            'data': {
                '$current_page': 4,
                '$total_pages': 10,
                '$boundaries': 12,  # boundaries is to height
                '$around': 2
            },
            'result': ''
        }
    ]

    def test_pagination_success(self):
        """Test success data."""
        for test_data in self.VALID_DATA:
            self.assertEqual(pagination(test_data['data']), test_data['result'])

    def test_pagination_fail(self):
        """Test invalid data."""
        for test_data in self.INVALID_DATA:
            self.assertEqual(pagination(test_data['data']), test_data['result'])
